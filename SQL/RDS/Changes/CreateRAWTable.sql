/****** Object:  Schema [Metrics]    Script Date: 10.01.2020 13:44:50 ******/
CREATE SCHEMA [Metrics]
GO

/****** Object:  Table [Metrics].[MetricsRawData]    Script Date: 10.01.2020 13:45:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Metrics].[MetricsRawData](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[app] [nvarchar](50) NOT NULL,
	[userlogin] [nvarchar](150) NOT NULL,
	[data] [xml] NOT NULL,
 CONSTRAINT [PK_MetricsRawData] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

