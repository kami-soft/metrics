unit Metrics.Classes;
{$I DelphiVersions.inc}

interface

uses
  System.Classes,
  System.Generics.Collections,
  System.Types,
  System.SyncObjs,
  Xml.XMLIntf,
  Metrics.Classes.Common,
  Metrics.Classes.Workstation,
  Metrics.Classes.Application,
  Metrics.Classes.UI;

type
  TFullMetricsInfo = class(TCustomInfo)
  strict private
    FWorkstationInfo: TWorkstationInfo;
    FApplicationInfo: TApplicationInfo;
    FFormInfo: TFormInfo;
    FActionInfo: TActionInfo;
    FFiredAt: TDateTime;
    FAdditionalInfo: TStrings;
  public
    constructor Create; override;
    destructor Destroy; override;

    procedure FillInfo(AAction: TBasicAction; AAdditionalInfo: TStrings);
    procedure FillInfoByComponent(AComponent: TComponent; AAdditionalInfo: TStrings);
    procedure SaveToXML(Node: IXMLNode); override;

    property FiredAt: TDateTime read FFiredAt;
    property WorkstationInfo: TWorkstationInfo read FWorkstationInfo;
    property ApplicationInfo: TApplicationInfo read FApplicationInfo;
    property FormInfo: TFormInfo read FFormInfo;
    property ActionInfo: TActionInfo read FActionInfo;
    property AdditionalInfo: TStrings read FAdditionalInfo;
  end;

  TFullInfoQueue = class(TObjectQueue<TFullMetricsInfo>)
  end;

  TMetricsThread = class(TThread)
  strict private
    class var FInstance: TMetricsThread;

    class function CanUseStatistic: Boolean;
    class function Instance: TMetricsThread;
    class destructor Destroy;
  strict private
    FCS: TCriticalSection;
    FEvent: TEvent;
    FQueue: TFullInfoQueue;

    procedure SendMetrics;

    procedure AddInfo(AAction: TBasicAction; AAdditionalInfo: TStrings);
    procedure AddInfoByComponent(AComponent: TComponent; AAdditionalInfo: TStrings);

  strict protected
    procedure Execute; override;
  public
    constructor Create;
    destructor Destroy; override;
  public
    class procedure DirectAddMetrics(AAction: TBasicAction; AAdditionalInfo: TStrings); overload;
    class procedure DirectAddMetrics(AAction: TBasicAction; const AAdditionalInfo: string); overload;
    class procedure DirectAddMetrics(AAction: TBasicAction; const AAdditionalInfo: array of string); overload;

    class procedure DirectAddMetricsByComponent(AComponent: TComponent; AAdditionalInfo: TStrings); overload;
    class procedure DirectAddMetricsByComponent(AComponent: TComponent; const AAdditionalInfo: string); overload;
    class procedure DirectAddMetricsByComponent(AComponent: TComponent; const AAdditionalInfo: array of string); overload;
  end;

implementation

uses
  System.SysUtils,
  System.StrUtils,
  System.DateUtils,
  Data.DB,
  Data.Win.ADODB,
  Winapi.ActiveX,
  Xml.XMLDoc,
  Common.Interfaces,
  Utils.GlobalServices,
  Utils.ConnectionPool;

{ TFullMetricsInfo }

constructor TFullMetricsInfo.Create;
begin
  inherited;
  FWorkstationInfo := TWorkstationInfo.Create;
  FApplicationInfo := TApplicationInfo.Create;
  FFormInfo := TFormInfo.Create;
  FActionInfo := TActionInfo.Create;
  FAdditionalInfo := TStringList.Create;
end;

destructor TFullMetricsInfo.Destroy;
begin
  FAdditionalInfo.Free;
  FActionInfo.Free;
  FFormInfo.Free;
  FApplicationInfo.Free;
  FWorkstationInfo.Free;
  inherited;
end;

procedure TFullMetricsInfo.FillInfo(AAction: TBasicAction; AAdditionalInfo: TStrings);
begin
  FFiredAt := TTimeZone.Local.ToUniversalTime(Now);
  FFormInfo.FillInfo(AAction);
  FActionInfo.FillInfo(AAction);
  FAdditionalInfo.Clear;
  if Assigned(AAdditionalInfo) then
    FAdditionalInfo.Assign(AAdditionalInfo);
end;

procedure TFullMetricsInfo.FillInfoByComponent(AComponent: TComponent; AAdditionalInfo: TStrings);
begin
  FFiredAt := TTimeZone.Local.ToUniversalTime(Now);
  FFormInfo.FillInfoByComponent(AComponent);
  FActionInfo.FillInfoByComponent(AComponent);
  FAdditionalInfo.Clear;
  if Assigned(AAdditionalInfo) then
    FAdditionalInfo.Assign(AAdditionalInfo);
end;

procedure TFullMetricsInfo.SaveToXML(Node: IXMLNode);
var
  addNode: IXMLNode;
  i: Integer;
  s: string;
begin
  inherited;
  Node.AddChild('FiredAtUTC').Text := FormatDateTime('yyyy-mm-dd hh:nn:ss.zzz', FFiredAt);
  FWorkstationInfo.SaveToXML(Node.AddChild('WorkstationInfo'));
  FApplicationInfo.SaveToXML(Node.AddChild('ApplicationInfo'));
  FFormInfo.SaveToXML(Node.AddChild('FormInfo'));
  FActionInfo.SaveToXML(Node.AddChild('ActionInfo'));

  addNode := Node.AddChild('AdditionalInfo');
  for i := 0 to FAdditionalInfo.Count - 1 do
    begin
      s := FAdditionalInfo[i];
      if AnsiContainsText(s, FAdditionalInfo.NameValueSeparator) then
        addNode.AddChild(FAdditionalInfo.Names[i]).Text := FAdditionalInfo.ValueFromIndex[i]
      else
        addNode.AddChild('item').Text := FAdditionalInfo[i];
    end;
end;

{ TMetricsThread }

procedure TMetricsThread.AddInfo(AAction: TBasicAction; AAdditionalInfo: TStrings);
var
  fmi: TFullMetricsInfo;
begin
  fmi := TFullMetricsInfo.Create;
  try
    fmi.FillInfo(AAction, AAdditionalInfo);
    FCS.Enter;
    try
      FQueue.Enqueue(fmi);
      fmi := nil;
      if FQueue.Count > 1000 then
        FQueue.Dequeue;
    finally
      FCS.Leave;
    end;
  finally
    fmi.Free;
  end;

  FEvent.SetEvent;
end;

procedure TMetricsThread.AddInfoByComponent(AComponent: TComponent; AAdditionalInfo: TStrings);
var
  fmi: TFullMetricsInfo;
begin
  fmi := TFullMetricsInfo.Create;
  try
    fmi.FillInfoByComponent(AComponent, AAdditionalInfo);
    FCS.Enter;
    try
      FQueue.Enqueue(fmi);
      fmi := nil;
      if FQueue.Count > 1000 then
        FQueue.Dequeue;
    finally
      FCS.Leave;
    end;
  finally
    fmi.Free;
  end;

  FEvent.SetEvent;
end;

class function TMetricsThread.CanUseStatistic: Boolean;
var
  GlobalParams: IRDSParamsGlobal;
begin
  if Supports(GlobalServices, IRDSParamsGlobal, GlobalParams) then
    begin
      Result := not GlobalParams.ReadBoolParam('StatisticDenyCollect', False);
    end
  else
    Result := True;
end;

constructor TMetricsThread.Create;
begin
  inherited Create(False);
  FCS := TCriticalSection.Create;
  FEvent := TEvent.Create;
  FQueue := TFullInfoQueue.Create;
end;

class destructor TMetricsThread.Destroy;
begin
  FInstance.Free;
  FInstance := nil;
end;

destructor TMetricsThread.Destroy;
begin
  Terminate;

  FCS.Enter;
  try
    FQueue.Clear;
  finally
    FCS.Leave;
  end;

  if Assigned(FEvent) then
    FEvent.SetEvent;
  if { Started and } (not Finished) then
    WaitFor;

  FEvent.Free;
  FCS.Free;
  FQueue.Free;
  inherited;
end;

class procedure TMetricsThread.DirectAddMetrics(AAction: TBasicAction; const AAdditionalInfo: string);
begin
  DirectAddMetrics(AAction, [AAdditionalInfo]);
end;

class procedure TMetricsThread.DirectAddMetrics(AAction: TBasicAction; AAdditionalInfo: TStrings);
var
  inst: TMetricsThread;
begin
  inst:=Instance;
  if Assigned(inst) then
    inst.AddInfo(AAction, AAdditionalInfo);
end;

class procedure TMetricsThread.DirectAddMetrics(AAction: TBasicAction; const AAdditionalInfo: array of string);
var
  SL: TStringList;
  i: Integer;
begin
  SL := TStringList.Create;
  try
    for i := low(AAdditionalInfo) to high(AAdditionalInfo) do
      SL.Add(AAdditionalInfo[i]);
    DirectAddMetrics(AAction, SL);
  finally
    SL.Free;
  end;
end;

class procedure TMetricsThread.DirectAddMetricsByComponent(AComponent: TComponent; const AAdditionalInfo: array of string);
var
  SL: TStringList;
  i: Integer;
begin
  SL := TStringList.Create;
  try
    for i := low(AAdditionalInfo) to high(AAdditionalInfo) do
      SL.Add(AAdditionalInfo[i]);
    DirectAddMetricsByComponent(AComponent, SL);
  finally
    SL.Free;
  end;
end;

class procedure TMetricsThread.DirectAddMetricsByComponent(AComponent: TComponent; const AAdditionalInfo: string);
begin
  DirectAddMetricsByComponent(AComponent, [AAdditionalInfo]);
end;

class procedure TMetricsThread.DirectAddMetricsByComponent(AComponent: TComponent; AAdditionalInfo: TStrings);
var
  inst: TMetricsThread;
begin
  inst:=Instance;
  if Assigned(inst) then
    inst.AddInfoByComponent(AComponent, AAdditionalInfo);
end;

procedure TMetricsThread.Execute;
begin
  CoInitialize(nil);
  try
    while not Terminated do
      begin
        FEvent.WaitFor;
        if Terminated then
          Break;

        try
          SendMetrics;
        except
          Sleep(0);
        end;
        FEvent.ResetEvent;
      end;
  finally
    CoUninitialize;
  end;
end;

class function TMetricsThread.Instance: TMetricsThread;
var
  bCanUseStatistic: Boolean;
begin
  bCanUseStatistic := CanUseStatistic;
  if (not Assigned(FInstance)) and bCanUseStatistic then
    FInstance := Self.Create;
  if not bCanUseStatistic then
    begin
      FInstance.Free;
      FInstance := nil;
    end;

  Result := FInstance;
end;

procedure TMetricsThread.SendMetrics;
  function TryDequeue: TFullMetricsInfo;
  begin
    Result := nil;
    FCS.Enter;
    try
      if (not Terminated) and (FQueue.Count <> 0) then
        Result := FQueue.Extract;
    finally
      FCS.Leave;
    end;
  end;

var
  item: TFullMetricsInfo;
  SP: TADOStoredProc;
  Xml: IXMLDocument;
  s: string;
begin
  while not Terminated do
    begin
      item := TryDequeue;
      if Assigned(item) then
        try
          SP := TADOStoredProc.Create(nil);
          try
            SP.Connection := TConnectionPool.Instance.GetThreadConnection(cstMain);
            try
              { try write item to DB }
              SP.ProcedureName := 'Metrics.spWriteRAWData';
              SP.Parameters.Refresh;

              Xml := NewXMLDocument;
              item.SaveToXML(Xml.AddChild('metrics'));
              Xml.SaveToXML(s);

              SP.Parameters.ParamByName('@app').Value := item.ApplicationInfo.ProgramRegisteredName;
              SP.Parameters.ParamByName('@userlogin').Value := item.ApplicationInfo.UserInfo.Login;
              SP.Parameters.ParamByName('@data').Value := s;
              SP.ExecProc;
            except
              SP.Connection.Connected := False;
            end;
          finally
            SP.Free;
          end;
        finally
          item.Free;
        end
      else
        Break;
    end;
end;

end.
