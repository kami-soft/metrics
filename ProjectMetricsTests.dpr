program ProjectMetricsTests;

uses
  madExcept,
  madLinkDisAsm,
  madListHardware,
  madListProcesses,
  madListModules,
  Vcl.Forms,
  ufmMetricsTest in 'ufmMetricsTest.pas' {Form17},
  Metrics.Classes.Workstation in 'Metrics.Classes.Workstation.pas',
  Metrics.Classes.Application in 'Metrics.Classes.Application.pas',
  Metrics.Classes.Common in 'Metrics.Classes.Common.pas',
  Metrics.Classes.UI in 'Metrics.Classes.UI.pas',
  Metrics.Classes in 'Metrics.Classes.pas',
  Metrics in 'Metrics.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm17, Form17);
  Application.Run;

end.
