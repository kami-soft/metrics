unit Metrics.Classes.Application;
{$I DelphiVersions.inc}
interface

uses
  System.Classes,
  System.Generics.Collections,
  System.Types,
  Xml.XMLIntf,
  {$IFDEF DXUp}
  System.JSON.Serializers,
  {$ENDIF}
  Metrics.Classes.Common;

type
  TVersion = packed record
    constructor Create(const AStr: string);
    function ToString: string;
    procedure FromString(const AStr: string);

    function CompareTo(const AVer: TVersion): integer;
    case integer of
      0:
        (Build, Release, Minor, Major: Word);
      1:
        ({$IFDEF DXUp}[JsonIgnore]{$ENDIF} DetailedItems: array [0 .. 3] of Word);
      2:
        ({$IFDEF DXUp}[JsonIgnore]{$ENDIF} HiVer, LoVer: Cardinal);
      3:
        ({$IFDEF DXUp}[JsonIgnore]{$ENDIF} Items: array [0 .. 1] of Cardinal);
      4:
        ({$IFDEF DXUp}[JsonIgnore]{$ENDIF} FullVer: UInt64);
  end;

  TModuleVersionInfo = class(TCustomInfo)
  strict private
    FModuleVersion: TVersion;
    FModuleName: string;
    FhModuleInstance: HMODULE;
    procedure ReadFileModuleVersion;
  public
    procedure Fill(hModuleInstance: HMODULE);
    procedure SaveToXML(Node: IXMLNode); override;
    property ModuleInstance: HMODULE read FhModuleInstance;
    property ModuleName: string read FModuleName;
    property ModuleVersion: TVersion read FModuleVersion;
  end;

  TModuleVersionInfoList = class(TInfoList<TModuleVersionInfo>)
  public
    function FindModuleByHInstance(hModuleInstance: HMODULE): TModuleVersionInfo;
    procedure FillInfo; override;
  end;

  TAppUserInfo = class(TCustomInfo)
  strict private
    FCOP: integer;
    FLogin: string;
    FName: string;
    FName_E: string;
    class function CanUsePrivateUserInfo: Boolean;
  public
    procedure FillInfo;
    procedure SaveToXML(Node: IXMLNode); override;

    property COP: integer read FCOP;
    property Login: string read FLogin;
    property Name: string read FName;
    property Name_E: string read FName_E;
  end;

  TApplicationInfo = class(TCustomInfo)
  strict private
    class var FModuleVersionInfoList: TModuleVersionInfoList;
    class destructor Destroy;
  strict private
    FFileName: string;
    FTittle: string;
    FUserInfo: TAppUserInfo;
    FProgramRegisteredName: string;
    procedure FillInfo;
    function GetModuleVersionInfoList: TModuleVersionInfoList;
  public
    constructor Create; override;
    destructor Destroy; override;
    procedure SaveToXML(Node: IXMLNode); override;

    property FileName: string read FFileName;
    property Tittle: string read FTittle;
    property ProgramRegisteredName: string read FProgramRegisteredName;
    property ModuleVersionInfoList: TModuleVersionInfoList read GetModuleVersionInfoList;
    property UserInfo: TAppUserInfo read FUserInfo;
  end;

implementation

uses
  System.SysUtils,
  System.StrUtils,
  Winapi.Windows,
  Winapi.PsAPI,
  Common.Interfaces,
  Utils.GlobalServices;

{ TVersion }

function TVersion.CompareTo(const AVer: TVersion): integer;
begin
  if Self.FullVer > AVer.FullVer then
    Result := 1
  else
    if Self.FullVer < AVer.FullVer then
      Result := -1
    else
      Result := 0;
end;

constructor TVersion.Create(const AStr: string);
begin
  inherited;
  FromString(AStr);
end;

procedure TVersion.FromString(const AStr: string);
var
  SIP: string;
  Start: integer;
  i: integer;
  Index: integer;
  Count: integer;
  SGroup: string;
  G: integer;
begin
  FullVer := 0;
  SIP := AStr + '.';
  Start := 1;
  for i := 3 downto 0 do
    begin
      index := PosEx('.', SIP, Start);
      if index = 0 then
        Break;
      Count := index - Start + 1;
      SGroup := Copy(SIP, Start, Count - 1);
      if TryStrToInt(SGroup, G) and (G >= 0) and (G <= MAXWORD) then
        DetailedItems[i] := G;
      Inc(Start, Count);
    end;
end;

function TVersion.ToString: string;
begin
  Result := Format('%d.%d.%d.%d', [Major, Minor, Release, Build]);
end;

{ TModuleVersionInfo }

procedure TModuleVersionInfo.Fill(hModuleInstance: HMODULE);
var
  nSize: Cardinal;
begin
  FhModuleInstance := hModuleInstance;
  SetLength(FModuleName, MAX_PATH + 1);
  nSize := GetModuleFileName(hModuleInstance, @FModuleName[1], MAX_PATH + 1);
  SetLength(FModuleName, nSize);
  ReadFileModuleVersion;
end;

procedure TModuleVersionInfo.ReadFileModuleVersion;
var
  Info: Pointer;
  InfoSize: dword;
  FileInfo: PVSFixedFileInfo;
  FileInfoSize: dword;
  Tmp: dword;
begin
  InfoSize := GetFileVersionInfoSize(PChar(FModuleName), Tmp);

  if InfoSize > 0 then
    begin
      GetMem(Info, InfoSize);
      try
        if GetFileVersionInfo(PChar(FModuleName), 0, InfoSize, Info) then
          begin
            VerQueryValue(Info, '\', Pointer(FileInfo), FileInfoSize);
            FModuleVersion.Items[1] := FileInfo.dwFileVersionMS;
            FModuleVersion.Items[0] := FileInfo.dwFileVersionLS;
          end;
      finally
        FreeMem(Info, FileInfoSize);
      end;
    end;
end;

procedure TModuleVersionInfo.SaveToXML(Node: IXMLNode);
begin
  inherited;
  Node.AddChild('ModuleName').Text := FModuleName;
  Node.AddChild('ModuleVersion').Text := FModuleVersion.ToString;
end;

{ TModuleVersionInfoList }

procedure TModuleVersionInfoList.FillInfo;
var
  HModulesArray: array [0 .. 1024] of HMODULE;
  HModules: TList<HMODULE>;
  cbNeeded: Cardinal;
  i: integer;
  Item: TModuleVersionInfo;
begin
  // inherited;  No inherited, because it contains Clear method!!!
  if EnumProcessModules(GetCurrentProcess, @HModulesArray[0], SizeOf(HModulesArray), cbNeeded) then
    begin
      HModules := TList<HMODULE>.Create;
      try
        cbNeeded := (cbNeeded div SizeOf(HMODULE));
        HModules.Capacity := cbNeeded;
        for i := 0 to cbNeeded - 1 do
          HModules.Add(HModulesArray[i]);

        // delete unneeded
        for i := Count - 1 downto 0 do
          begin
            if not HModules.Contains(Items[i].ModuleInstance) then
              Delete(i);
          end;

        // add unexisting
        for i := 0 to HModules.Count - 1 do
          if not Assigned(FindModuleByHInstance(HModulesArray[i])) then
            begin
              Item := TModuleVersionInfo.Create;
              try
                Item.Fill(HModulesArray[i]);
                Add(Item);
                Item := nil;
              finally
                Item.Free;
              end;
            end;
      finally
        HModules.Free;
      end;
    end;
end;

function TModuleVersionInfoList.FindModuleByHInstance(hModuleInstance: HMODULE): TModuleVersionInfo;
var
  i: integer;
begin
  Result := nil;
  for i := 0 to Count - 1 do
    if Items[i].ModuleInstance = hModuleInstance then
      begin
        Result := Items[i];
        Break;
      end;
end;

{ TAppUserInfo }

class function TAppUserInfo.CanUsePrivateUserInfo: Boolean;
var
  GlobalParams: IRDSParamsGlobal;
begin
  if Supports(GlobalServices, IRDSParamsGlobal, GlobalParams) then
    begin
      Result := not GlobalParams.ReadBoolParam('StatisticDenyPrivateInfo', False);
    end
  else
    Result := True;
end;

procedure TAppUserInfo.FillInfo;
var
  ui: IUserInfo;
begin
  if Supports(GlobalServices, IUserInfo, ui) and CanUsePrivateUserInfo then
    begin
      FCOP := ui.COP;
      FLogin := ui.Login;
      FName := ui.name;
      FName_E := ui.Name_E;
    end;
end;

procedure TAppUserInfo.SaveToXML(Node: IXMLNode);
begin
  inherited;
  Node.AddChild('COP').Text := IntToStr(FCOP);
  Node.AddChild('Login').Text := FLogin;
  Node.AddChild('Name').Text := FName;
  Node.AddChild('Name_E').Text := FName_E;
end;

{ TApplicationInfo }

constructor TApplicationInfo.Create;
begin
  inherited;
  FUserInfo := TAppUserInfo.Create;

  FillInfo;
end;

destructor TApplicationInfo.Destroy;
begin
  FUserInfo.Free;
  inherited;
end;

class destructor TApplicationInfo.Destroy;
begin
  FModuleVersionInfoList.Free;
end;

procedure TApplicationInfo.FillInfo;
var
  psd: IProgramSpecificData;
begin
  FFileName := ParamStr(0);
  if Supports(GlobalServices, IProgramSpecificData, psd) then
    begin
      FTittle := psd.ProgrammDescription;
      FProgramRegisteredName := psd.ProgrammRegisteredName;
    end;
  if not Assigned(FModuleVersionInfoList) then
    FModuleVersionInfoList := TModuleVersionInfoList.Create;
  FModuleVersionInfoList.FillInfo;
  FUserInfo.FillInfo;
end;

function TApplicationInfo.GetModuleVersionInfoList: TModuleVersionInfoList;
begin
  Result := FModuleVersionInfoList;
end;

procedure TApplicationInfo.SaveToXML(Node: IXMLNode);
begin
  inherited;
  Node.AddChild('FileName').Text := FFileName;
  Node.AddChild('Tittle').Text := FTittle;
  Node.AddChild('RegisteredName').Text := FProgramRegisteredName;
  FModuleVersionInfoList.SaveToXML(Node.AddChild('Modules'));
  FUserInfo.SaveToXML(Node.AddChild('User'));
end;

end.
