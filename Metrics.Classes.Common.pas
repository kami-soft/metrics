unit Metrics.Classes.Common;
{$I DelphiVersions.inc}
interface

uses
  System.Classes,
  System.Generics.Collections,
  System.Types,
  Xml.XMLIntf
  {$IFDEF DXUp}
  ,System.JSON.Types,
  System.JSON.Serializers
  {$ENDIF};

type
  {$IFDEF DXUP}
  [JsonSerialize(TJsonMemberSerialization.&Public)]
  {$ENDIF}

  TCustomInfo = class(TPersistent)
  public
    constructor Create; virtual;
    procedure SaveToXML(Node: IXMLNode); virtual;

    class procedure SaveRectToXML(Node: IXMLNode; const Rect: TRect);
  end;

  {$IFDEF DXUp}
  [JsonSerialize(TJsonMemberSerialization.&Public)]
  {$ENDIF}

  TInfoList<T: TCustomInfo, constructor> = class(TObjectList<T>)
  public
    procedure FillInfo; virtual;
    procedure SaveToXML(Node: IXMLNode); virtual;
  end;

implementation
uses
  System.SysUtils;

{ TCustomInfo }

constructor TCustomInfo.Create;
begin
  inherited Create;
end;

class procedure TCustomInfo.SaveRectToXML(Node: IXMLNode; const Rect: TRect);
begin
  Node.AddChild('Left').Text := IntToStr(Rect.Left);
  Node.AddChild('Top').Text := IntToStr(Rect.Top);
  Node.AddChild('Right').Text := IntToStr(Rect.Right);
  Node.AddChild('Bottom').Text := IntToStr(Rect.Bottom);
  Node.AddChild('Width').Text := IntToStr(Rect.Width);
  Node.AddChild('Height').Text := IntToStr(Rect.Height);
end;

procedure TCustomInfo.SaveToXML(Node: IXMLNode);
begin
  Node.Attributes['ClassName'] := Self.ClassName;
end;

{ TInfoList<T> }

procedure TInfoList<T>.FillInfo;
begin
  Clear;
end;

procedure TInfoList<T>.SaveToXML(Node: IXMLNode);
var
  i: Integer;
begin
  Node.Attributes['ClassName'] := Self.ClassName;
  for i := 0 to Count - 1 do
    Items[i].SaveToXML(Node.AddChild('Item'));
end;

end.
