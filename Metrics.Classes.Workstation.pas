unit Metrics.Classes.Workstation;
{$I DelphiVersions.inc}

interface

uses
  System.Classes,
  System.Generics.Collections,
  System.SysUtils,
  System.Types,
  Xml.XMLIntf,
  {$IFDEF DXUp}
  System.JSON.Serializers,
  {$ENDIF}
  uSMBIOS,
  Metrics.Classes.Common;

type
  TMonitorInfo = class(TCustomInfo)
  strict private
    FMonitorRect: TRect;

    function GetMonitorWidth: Integer;
    function GetMonitorHeight: Integer;
  public
    procedure Fill(const Rect: TRect);
    procedure SaveToXML(Node: IXMLNode); override;

    property MonitorLeft: Integer read FMonitorRect.Left;
    property MonitorTop: Integer read FMonitorRect.Top;
    property MonitorRight: Integer read FMonitorRect.Right;
    property MonitorBottom: Integer read FMonitorRect.Bottom;
    property MonitorWidth: Integer read GetMonitorWidth;
    property MonitorHeight: Integer read GetMonitorHeight;
  end;

  TMonitorInfoList = class(TInfoList<TMonitorInfo>)
  public
    procedure FillInfo; override;
  end;

  TCPUInfo = class(TCustomInfo)
  strict private
    FCPUName: string;
  public
    procedure Fill(ProcessorInfo: TProcessorInformation);
    procedure SaveToXML(Node: IXMLNode); override;

    property CPUName: string read FCPUName;
  end;

  TCPUInfoList = class(TInfoList<TCPUInfo>)
  public
    procedure Fill(smb: TSMBios);
    procedure FillInfo; override;
  end;

  TDriveInfo = class(TCustomInfo)
  strict private
    FTotalSize: Int64;
    FFreeSize: Int64;
    FDriveLetter: Char;
  public
    procedure FillInfo(const ADriveLetter: Char);
    procedure SaveToXML(Node: IXMLNode); override;

    property DriveLetter: Char read FDriveLetter;
    property TotalSize: Int64 read FTotalSize;
    property FreeSize: Int64 read FFreeSize;
  end;

  {$IFDEF DXUp}
  [JsonSerialize(TJsonMemberSerialization.&Public)]
  {$ENDIF}

  TDriveInfoList = class(TInfoList<TDriveInfo>)
  public
    procedure FillInfo; override;
  end;

  TOSInfo = class(TCustomInfo)
  private
    FOSName: string;
    FArchitecture: TOSVersion.TArchitecture;

    FMajor: Integer;
    FMinor: Integer;
    FBuild: Integer;

    FName: string;
    FPlatform: TOSVersion.TPlatform;

    FServicePackMajor: Integer;
    FServicePackMinor: Integer;

    procedure FillInfo;
  public
    constructor Create; override;

    procedure SaveToXML(Node: IXMLNode); override;

    property OSName: string read FOSName;
    property Architecture: TOSVersion.TArchitecture read FArchitecture;
    property Build: Integer read FBuild;
    property Major: Integer read FMajor;
    property Minor: Integer read FMinor;
    property name: string read FName;
    property sPlatform: TOSVersion.TPlatform read FPlatform;
    property ServicePackMajor: Integer read FServicePackMajor;
    property ServicePackMinor: Integer read FServicePackMinor;
  end;

  TWorkstationInfo = class(TCustomInfo)
  strict private
  class var
    FRAMMb: Int64;
    FCPUInfoList: TCPUInfoList;
    FOSInfo: TOSInfo;
    FDriveInfoList: TDriveInfoList;
    FMonitorInfoList: TMonitorInfoList;
    FUserName: string;
    FWorkstationName: string;
    class destructor Destroy;
    class function CanUsePrivateUserInfo: Boolean;
  strict private
    procedure FillInfo;
    function GetWorkstationName: string;
    function GetUserName: string;
    function GetOSInfo: TOSInfo;
    function GetRAMMb: Int64;
    function GEtCPUInfoList: TCPUInfoList;
    function GetDriveInfoList: TDriveInfoList;
    function GetMonitorInfoList: TMonitorInfoList;
  public
    constructor Create; override;

    procedure SaveToXML(Node: IXMLNode); override;
    {$IFDEF DXUP}
    [JsonName('WorkstationName')]
    [JsonIn]
    {$ENDIF}
    property WorkstationName: string read GetWorkstationName;
    property UserName: string read GetUserName;
    property OSInfo: TOSInfo read GetOSInfo;
    property RAMMb: Int64 read GetRAMMb;
    property CPUInfoList: TCPUInfoList read GEtCPUInfoList;
    property DriveInfoList: TDriveInfoList read GetDriveInfoList;
    property MonitorInfoList: TMonitorInfoList read GetMonitorInfoList;
  end;

implementation

uses
  Winapi.Windows,
  Utils.Conversation,
  Common.Interfaces,
  Utils.GlobalServices;

const
  NameUnknown = 0;
  NameFullyQualifiedDN = 1;
  NameSamCompatible = 2;
  NameDisplay = 3;
  NameUniqueId = 6;
  NameCanonical = 7;
  NameUserPrincipal = 8;
  NameCanonicalEx = 9;
  NameServicePrincipal = 10;
  NameDnsDomain = 12;

function GetUserNameExW(NameFormat: DWORD; lpNameBuffer: LPWSTR; var nSize: ULONG): ByteBool; stdcall; external 'secur32.dll';

function GetUserNameExString(ANameFormat: DWORD): WideString;
var
  nSize: DWORD;
begin
  nSize := 1024;
  SetLength(Result, nSize);

  if GetUserNameExW(ANameFormat, PWideChar(Result), nSize) then
    SetLength(Result, nSize)
  else
    Result := '';
end;

function GetComputerNameExString(ANameFormat: COMPUTER_NAME_FORMAT): WideString;
var
  nSize: DWORD;
begin
  nSize := 1024;
  SetLength(Result, nSize);
  if GetComputerNameExW(ANameFormat, PWideChar(Result), nSize) then
    SetLength(Result, nSize)
  else
    Result := '';
end;

type
  HMONITOR = THandle;
  LPRECT = PRect;
  LPCRECT = PRect;
  MONITORENUMPROC = function(Monitor: HMONITOR; hdcMonitor: HDC; lprcMonitor: LPRECT; dwData: LPARAM): BOOL; stdcall;

  MONITORINFO = record
    cbSize: DWORD;
    rcMonitor: TRect;
    rcWork: TRect;
    dwFlags: DWORD;
  end;

  LPMONITORINFO = ^MONITORINFO;

function EnumDisplayMonitors(HDC: HDC; lprcClip: LPCRECT; lpfnEnum: MONITORENUMPROC; dwData: LPARAM): BOOL; stdcall; external 'user32.dll';
function GetMonitorInfoW(HMONITOR: HMONITOR; lpmi: LPMONITORINFO): BOOL; stdcall; external 'user32.dll';

{ TMonitorInfo }

procedure TMonitorInfo.Fill(const Rect: TRect);
begin
  FMonitorRect := Rect;
end;

function TMonitorInfo.GetMonitorHeight: Integer;
begin
  Result := FMonitorRect.Height;
end;

function TMonitorInfo.GetMonitorWidth: Integer;
begin
  Result := FMonitorRect.Width;
end;

procedure TMonitorInfo.SaveToXML(Node: IXMLNode);
begin
  inherited;
  SaveRectToXML(Node.AddChild('MonitorBounds'), FMonitorRect);
end;

{ TMonitorInfoList }

function MonitorEnumCallbackProc(Monitor: HMONITOR; hdcMonitor: HDC; lprcMonitor: LPRECT; dwData: LPARAM): BOOL; stdcall;
var
  MonitorInfoList: TMonitorInfoList;
  Item: TMonitorInfo;
begin
  Result := True;
  MonitorInfoList := TMonitorInfoList(Pointer(dwData));

  Item := TMonitorInfo.Create;
  try
    Item.Fill(lprcMonitor^);
    MonitorInfoList.Add(Item);
    Item := nil;
  finally
    Item.Free;
  end;
end;

procedure TMonitorInfoList.FillInfo;
begin
  inherited;
  EnumDisplayMonitors(0, nil, MonitorEnumCallbackProc, LPARAM(Self));
end;

{ TCPUInfo }

procedure TCPUInfo.Fill(ProcessorInfo: TProcessorInformation);
begin
  FCPUName := Format('%s', [ProcessorInfo.ProcessorVersionStr]);
end;

procedure TCPUInfo.SaveToXML(Node: IXMLNode);
begin
  inherited;
  Node.AddChild('CPUName').Text := FCPUName;
end;

{ TCPUInfoList }

procedure TCPUInfoList.Fill(smb: TSMBios);
var
  i: Integer;
  Item: TCPUInfo;
begin
  Clear;
  if smb.HasProcessorInfo then
    for i := low(smb.ProcessorInfo) to high(smb.ProcessorInfo) do
      begin
        Item := TCPUInfo.Create;
        try
          Item.Fill(smb.ProcessorInfo[i]);
          Add(Item);
          Item := nil;
        finally
          Item.Free;
        end;
      end;
end;

procedure TCPUInfoList.FillInfo;
var
  smb: TSMBios;
begin
  inherited;
  smb := TSMBios.Create;
  try
    Fill(smb);
  finally
    smb.Free;
  end;
end;

{ TDriveInfo }

procedure TDriveInfo.FillInfo(const ADriveLetter: Char);
begin
  FDriveLetter := ADriveLetter;
  FTotalSize := DiskSize(Byte(ADriveLetter) - $40);
  FFreeSize := DiskFree(Byte(ADriveLetter) - $40);
end;

procedure TDriveInfo.SaveToXML(Node: IXMLNode);
begin
  inherited;
  Node.AddChild('DriveLetter').Text := FDriveLetter;
  Node.AddChild('TotalSize').Text := IntToStr(FTotalSize);
  Node.AddChild('FreeSize').Text := IntToStr(FFreeSize);
end;

{ TDriveInfoList }

procedure TDriveInfoList.FillInfo;
const
  DRIVE_UNKNOWN = 0;
  DRIVE_NO_ROOT_DIR = 1;
  DRIVE_REMOVABLE = 2;
  DRIVE_FIXED = 3;
  DRIVE_REMOTE = 4;
  DRIVE_CDROM = 5;
  DRIVE_RAMDISK = 6;
var
  r: LongWord;
  DriveLetters: array [0 .. 128] of Char;
  i: Integer;

  Drive: TDriveInfo;
begin
  inherited;
  r := GetLogicalDriveStrings(SizeOf(DriveLetters), DriveLetters);
  if r = 0 then
    exit;
  if r > SizeOf(DriveLetters) then
    raise Exception.Create(SysErrorMessage(ERROR_OUTOFMEMORY));

  i := 0;
  while i < Length(DriveLetters) do
    begin
      if GetDriveType(@DriveLetters[i]) = DRIVE_FIXED then
        begin
          Drive := TDriveInfo.Create;
          try
            Drive.FillInfo(DriveLetters[i]);
            Add(Drive);
            Drive := nil;
          finally
            Drive.Free;
          end;
        end;
      Inc(i, 4);
      if DriveLetters[i] = #0 then
        Break;
    end;
end;

{ TWorkstation }

class function TWorkstationInfo.CanUsePrivateUserInfo: Boolean;
var
  GlobalParams: IRDSParamsGlobal;
begin
  if Supports(GlobalServices, IRDSParamsGlobal, GlobalParams) then
    begin
      Result := not GlobalParams.ReadBoolParam('StatisticDenyPrivateInfo', False);
    end
  else
    Result := True;
end;

constructor TWorkstationInfo.Create;
begin
  inherited;

  if not Assigned(FCPUInfoList) then
    begin
      FCPUInfoList := TCPUInfoList.Create;
      FMonitorInfoList := TMonitorInfoList.Create;
      FDriveInfoList := TDriveInfoList.Create;
      FOSInfo := TOSInfo.Create;
    end;

  FillInfo;
end;

class destructor TWorkstationInfo.Destroy;
begin
  FOSInfo.Free;
  FCPUInfoList.Free;
  FDriveInfoList.Free;
  FMonitorInfoList.Free;
end;

procedure TWorkstationInfo.FillInfo;
var
  smb: TSMBios;
  i: Integer;
begin
  FDriveInfoList.FillInfo;

  if FWorkstationName <> '' then
    exit;
  FWorkstationName := GetComputerNameExString(ComputerNamePhysicalDnsFullyQualified);
  if CanUsePrivateUserInfo then
    FUserName := GetUserNameExString(NameSamCompatible)
  else
    FUserName := '';

  smb := TSMBios.Create;
  try
    if smb.HasMemoryDeviceInfo then
      for i := low(smb.MemoryDeviceInfo) to high(smb.MemoryDeviceInfo) do
        FRAMMb := FRAMMb + smb.MemoryDeviceInfo[i].GetSize;

    FCPUInfoList.Fill(smb);
  finally
    smb.Free;
  end;

  FMonitorInfoList.FillInfo;
end;

function TWorkstationInfo.GEtCPUInfoList: TCPUInfoList;
begin
  Result := FCPUInfoList;
end;

function TWorkstationInfo.GetDriveInfoList: TDriveInfoList;
begin
  Result := FDriveInfoList;
end;

function TWorkstationInfo.GetMonitorInfoList: TMonitorInfoList;
begin
  Result := FMonitorInfoList;
end;

function TWorkstationInfo.GetOSInfo: TOSInfo;
begin
  Result := FOSInfo;
end;

function TWorkstationInfo.GetRAMMb: Int64;
begin
  Result := FRAMMb;
end;

function TWorkstationInfo.GetUserName: string;
begin
  Result := FUserName;
end;

function TWorkstationInfo.GetWorkstationName: string;
begin
  Result := FWorkstationName;
end;

procedure TWorkstationInfo.SaveToXML(Node: IXMLNode);
begin
  inherited;
  Node.AddChild('WorkstationName').Text := FWorkstationName;
  FOSInfo.SaveToXML(Node.AddChild('OSInfo'));
  Node.AddChild('RAMMb').Text := IntToStr(FRAMMb);
  Node.AddChild('OSUser').Text := FUserName;
  FCPUInfoList.SaveToXML(Node.AddChild('CPU'));
  FDriveInfoList.SaveToXML(Node.AddChild('Drives'));
  FMonitorInfoList.SaveToXML(Node.AddChild('Monitors'));
end;

{ TOSInfo }

constructor TOSInfo.Create;
begin
  inherited;
  FillInfo;
end;

procedure TOSInfo.FillInfo;
begin
  FOSName := TOSVersion.ToString;
  FArchitecture := TOSVersion.Architecture;
  FMajor := TOSVersion.Major;
  FMinor := TOSVersion.Minor;
  FBuild := TOSVersion.Build;
  FName := TOSVersion.Name;
  FPlatform := TOSVersion.Platform;
  FServicePackMajor := TOSVersion.ServicePackMajor;
  FServicePackMinor := TOSVersion.ServicePackMinor;
end;

procedure TOSInfo.SaveToXML(Node: IXMLNode);
begin
  inherited;
  Node.AddChild('OSName').Text := FOSName;
  Node.AddChild('Architecture').Text := IntToStr(Integer(FArchitecture));
  Node.AddChild('ArchitectureText').Text := TEnumeration<TOSVersion.TArchitecture>.ToString(FArchitecture);
  Node.AddChild('Major').Text := IntToStr(FMajor);
  Node.AddChild('Minor').Text := IntToStr(FMinor);
  Node.AddChild('Build').Text := IntToStr(FBuild);
  Node.AddChild('Name').Text := FName;
  Node.AddChild('Platform').Text := IntToStr(Integer(FPlatform));
  Node.AddChild('PlatformStr').Text := TEnumeration<TOSVersion.TPlatform>.ToString(FPlatform);
  Node.AddChild('ServicePackMajor').Text := IntToStr(FServicePackMajor);
  Node.AddChild('ServicePackMinor').Text := IntToStr(FServicePackMinor);
end;

end.
