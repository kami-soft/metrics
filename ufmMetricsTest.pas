unit ufmMetricsTest;
{$I DelphiVersions.inc}
interface

uses
  Winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Variants,
  System.Classes,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs,
  Vcl.StdCtrls,
  Vcl.ActnList,
  Vcl.ExtCtrls,
  Metrics, System.Actions;

type
  TForm17 = class(TForm)
    btn1: TButton;
    mmo1: TMemo;
    btn2: TButton;
    actlst1: TActionList;
    act1: TAction;
    pnl1: TPanel;
    procedure act1Execute(Sender: TObject);
    procedure actlst1Execute(Action: TBasicAction; var Handled: Boolean);
    procedure btn2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form17: TForm17;

implementation

uses
  System.DateUtils,
  {$IFDEF DXUP}
  System.JSON.Types,
  System.JSON.Serializers,
  System.JSON.Converters,
  {$ENDIF}
  uSMBIOS,
  Xml.XMLIntf,
  Xml.XMLDoc,
  Data.Win.ADODB,
  Common.Interfaces,
  Utils.GlobalServices,
  Metrics.Classes.Workstation,
  Metrics.Classes.Application,
  Metrics.Classes;

{$R *.dfm}

procedure TForm17.act1Execute(Sender: TObject);
{var
  obj: TFullMetricsInfo;
  dt1, dt2: TDateTime;
  i: Integer;
  delta: Int64;
  Xml: IXMLDocument;
  s: string;  }
begin
  {dt1 := Now;
  s := '';
  for i := 0 to 999 do
  begin
    obj := TFullMetricsInfo.Create;
    try
      obj.FillInfo(Sender as TBasicAction, nil);
      Xml := NewXMLDocument;
      obj.SaveToXML(Xml.AddChild('metrics'));
      Xml.SaveToXML(s);
      Xml:=nil;
    finally
      obj.Free;
    end;
  end;

  mmo1.Text := s;
  dt2 := Now;

  delta := MilliSecondsBetween(dt1, dt2);
  mmo1.Lines.Add('milliseconds = ' + IntToStr(delta));
  mmo1.Lines.Add('avg per item = ' + IntToStr(delta div 1000));}
end;

procedure TForm17.actlst1Execute(Action: TBasicAction; var Handled: Boolean);
begin
  if Action = act1 then
    actlst1.AdditionalInfo.Add('bla-bla-bla');
end;

procedure TForm17.btn2Click(Sender: TObject);
{$IFDEF DXUP}
var
  Serializer: TJsonSerializer;
  obj: TWorkstationInfo;
  i: Integer;
  {$ENDIF}
begin
  {$IFDEF DXUP}
  Serializer := TJsonSerializer.Create;
  try
    Serializer.Formatting := TJSONFormatting.Indented;
    Serializer.Converters.Add(TJsonListConverter<TModuleVersionInfo>.Create);
    Serializer.Converters.Add(TJsonListConverter<TMonitorInfo>.Create);
    Serializer.Converters.Add(TJsonListConverter<TCPUInfo>.Create);
    Serializer.Converters.Add(TJsonListConverter<TDriveInfo>.Create);
    try
      obj := TWorkstationInfo.Create;
      try
        mmo1.Text := Serializer.Serialize<TWorkstationInfo>(obj);
      finally
        obj.Free;
      end;
    finally
      for i := 0 to Serializer.Converters.Count - 1 do
        Serializer.Converters[i].Free;
    end;
  finally
    Serializer.Free;
  end;
  {$ENDIF}
end;

procedure TForm17.FormCreate(Sender: TObject);
begin
  GlobalConnection.MainConnectionString:='Provider=SQLOLEDB.1;Password=pulkovo294;Persist Security Info=True;User ID=pulkovo;Initial Catalog=RDS;Data Source=srvr154';
  GlobalConnection.ConnectionString[cstRDS]:=GlobalConnection.MainConnectionString;
end;

procedure TForm17.FormDestroy(Sender: TObject);
begin
  FinalizeGlobalServices;
end;

end.
