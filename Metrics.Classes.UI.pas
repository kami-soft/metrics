unit Metrics.Classes.UI;
{$I DelphiVersions.inc}
interface

uses
  System.Classes,
  System.Generics.Collections,
  System.Types,
  Vcl.Controls,
  {$IFDEF DXUp}
  System.Actions,
  {$ELSE}
  Vcl.ActnList,
  {$ENDIF}
  Vcl.Forms,
  Xml.XMLIntf,
  {$IFDEF DXUp}
  System.JSON.Serializers,
  {$ENDIF}
  Metrics.Classes.Common;

type
  {$IFDEF DXUp}
  TMetricsAction = TContainedAction;
  {$ELSE}
  TMetricsAction = TCustomAction;
  {$ENDIF}

  TFormInfo = class(TCustomInfo)
  strict private
    FBounds: TRect;
    FCaption: string;
    FFormClassName: string;
    FScreenNo: integer;
    FWindowState: TWindowState;

    function FindParentFormByComponent(AComponent: TComponent): TCustomForm;
    function FindParentForm(AAction: TBasicAction): TCustomForm;
  public
    procedure FillInfo(AAction: TBasicAction);
    procedure FillInfoByComponent(AComponent: TComponent);
    procedure SaveToXML(Node: IXMLNode); override;

    property FormClassName: string read FFormClassName;
    property Caption: string read FCaption;
    property ScreenNo: integer read FScreenNo;
    property Bounds: TRect read FBounds;
    property WindowState: TWindowState read FWindowState;
  end;

  TComponentPathInfo = class(TCustomInfo)
  private
    FComponentName: string;
    FComponentClassName: string;
    FComponentCaption: string;
  public
    procedure FillInfo(AComponent: TComponent);
    procedure SaveToXML(Node: IXMLNode); override;

    function ToString: string; override;

    property ComponentName: string read FComponentName;
    property ComponentCaption: string read FComponentCaption;
    property ComponentClassName: string read FComponentClassName;
  end;

  TComponentPathInfoList = class(TInfoList<TComponentPathInfo>)
  public
    procedure FillComponentInfo(AComponent: TComponent);
    procedure FillControlInfo(AComponent: TComponent);

    function ToString: string; override;
  end;

  TActionComponentInfo = class(TCustomInfo)
  strict private
    FComponentInfo: TComponentPathInfo;
    FFullParentPath: TComponentPathInfoList;
    FFullOwnerPath: TComponentPathInfoList;
  public
    constructor Create; override;
    destructor Destroy; override;

    procedure FillInfo(AAction: TBasicAction);
    procedure FillInfoByComponent(AComponent: TComponent);
    procedure SaveToXML(Node: IXMLNode); override;

    property FullParentPath: TComponentPathInfoList read FFullParentPath;
    property FullOwnerPath: TComponentPathInfoList read FFullOwnerPath;
  end;

  TActionInfo = class(TCustomInfo)
  strict private
    FName: string;
    FCaption: string;
    FCategory: string;
    FShortCut: string;
    FSecondaryShortCut: string;
    FHint: string;
    FActionComponentInfo: TActionComponentInfo;
  public
    constructor Create; override;
    destructor Destroy; override;

    procedure FillInfo(AAction: TBasicAction);
    procedure FillInfoByComponent(AComponent: TComponent);
    procedure SaveToXML(Node: IXMLNode); override;

    property name: string read FName;
    property Caption: string read FCaption;
    property Category: string read FCategory;
    property ShortCut: string read FShortCut;
    property SecondaryShortCut: string read FSecondaryShortCut;
    property Hint: string read FHint;
    property ActionComponentInfo: TActionComponentInfo read FActionComponentInfo;
  end;

implementation

uses
  System.SysUtils,
  Utils.Conversation,
  Vcl.Menus;

{ TFormInfo }

procedure TFormInfo.FillInfo(AAction: TBasicAction);
var
  fm: TCustomForm;
begin
  fm := FindParentForm(AAction);
  if not Assigned(fm) then
    exit;
  FFormClassName := fm.ClassName;
  FCaption := fm.Caption;
  FWindowState := fm.WindowState;
  if Assigned(fm.Monitor) then
    FScreenNo := fm.Monitor.MonitorNum;
  FBounds := fm.BoundsRect;
end;

procedure TFormInfo.FillInfoByComponent(AComponent: TComponent);
var
  fm: TCustomForm;
begin
  fm := FindParentFormByComponent(AComponent);
  if not Assigned(fm) then
    exit;
  FFormClassName := fm.ClassName;
  FCaption := fm.Caption;
  FWindowState := fm.WindowState;
  if Assigned(fm.Monitor) then
    FScreenNo := fm.Monitor.MonitorNum;
  FBounds := fm.BoundsRect;
end;

function TFormInfo.FindParentForm(AAction: TBasicAction): TCustomForm;
begin
  Result := nil;
  if not Assigned(AAction) then
    exit;

  Result := FindParentFormByComponent(AAction.ActionComponent);
  if not Assigned(Result) then
    if AAction is TMetricsAction then
      Result := FindParentFormByComponent(TMetricsAction(AAction).GetParentComponent);
end;

function TFormInfo.FindParentFormByComponent(AComponent: TComponent): TCustomForm;
var
  ParentControl: TControl;
  OwnerComponent: TComponent;
begin
  Result := nil;
  if not Assigned(AComponent) then
    exit;

  if (AComponent is TControl) then
  begin
    ParentControl := TControl(AComponent);
    while Assigned(ParentControl) do
    begin
      if ParentControl is TCustomForm then
      begin
        Result := TCustomForm(ParentControl);
        Break;
      end;
      ParentControl := ParentControl.Parent;
    end;
  end;

  if not Assigned(Result) then
  begin
    OwnerComponent := AComponent;
    while Assigned(OwnerComponent) do
    begin
      if OwnerComponent is TCustomForm then
      begin
        Result := TCustomForm(OwnerComponent);
        Break;
      end;
      OwnerComponent := OwnerComponent.Owner;
    end;
  end;
end;

procedure TFormInfo.SaveToXML(Node: IXMLNode);
begin
  inherited;
  Node.AddChild('FormClassName').Text := FFormClassName;
  Node.AddChild('Caption').Text := FCaption;
  Node.AddChild('ScreenNo').Text := IntToStr(FScreenNo);
  Node.AddChild('WindowState').Text := IntToStr(integer(FWindowState));
  Node.AddChild('WindowStateText').Text := Tenumeration<TWindowState>.ToString(FWindowState);

  SaveRectToXML(Node.AddChild('Bounds'), FBounds);
end;

{ TActionComponentInfo }

constructor TActionComponentInfo.Create;
begin
  inherited;
  FComponentInfo := TComponentPathInfo.Create;
  FFullParentPath := TComponentPathInfoList.Create;
  FFullOwnerPath := TComponentPathInfoList.Create;
end;

destructor TActionComponentInfo.Destroy;
begin
  FFullOwnerPath.Free;
  FFullParentPath.Free;
  FComponentInfo.Free;
  inherited;
end;

procedure TActionComponentInfo.FillInfo(AAction: TBasicAction);
begin
  FillInfoByComponent(AAction.ActionComponent);
end;

procedure TActionComponentInfo.FillInfoByComponent(AComponent: TComponent);
begin
  if Assigned(AComponent) then
    FComponentInfo.FillInfo(AComponent);
  FFullParentPath.FillControlInfo(AComponent);
  FFullOwnerPath.FillComponentInfo(AComponent);
end;

procedure TActionComponentInfo.SaveToXML(Node: IXMLNode);
begin
  inherited;
  FComponentInfo.SaveToXML(Node.AddChild('ActionComponent'));
  FFullOwnerPath.SaveToXML(Node.AddChild('OwnerPath'));
  Node.AddChild('OwnerPathText').Text := FFullOwnerPath.ToString;
  FFullParentPath.SaveToXML(Node.AddChild('ParentPath'));
  Node.AddChild('ParentPathText').Text := FFullParentPath.ToString;
end;

{ TActionInfo }

constructor TActionInfo.Create;
begin
  inherited;
  FActionComponentInfo := TActionComponentInfo.Create;
end;

destructor TActionInfo.Destroy;
begin
  FActionComponentInfo.Free;
  inherited;
end;

procedure TActionInfo.FillInfo(AAction: TBasicAction);
var
  ca: TMetricsAction;
  i: integer;
begin
  if Assigned(AAction) then
  begin
    FName := AAction.Name;
    if AAction is TMetricsAction then
    begin
      ca := TMetricsAction(AAction);
      FCaption := ca.Caption;
      FCategory := ca.Category;
      FShortCut := ShortCutToText(ca.ShortCut);
      FSecondaryShortCut := '';
      for i := 0 to ca.SecondaryShortCuts.Count - 1 do
        FSecondaryShortCut := FSecondaryShortCut + ' / ' + ShortCutToText(ca.SecondaryShortCuts.ShortCuts[i]);
      FHint := ca.Hint;
    end;
    FActionComponentInfo.FillInfo(AAction);
  end;
end;

procedure TActionInfo.FillInfoByComponent(AComponent: TComponent);
begin
  FActionComponentInfo.FillInfoByComponent(AComponent);
end;

procedure TActionInfo.SaveToXML(Node: IXMLNode);
begin
  inherited;
  Node.AddChild('Name').Text := FName;
  Node.AddChild('Caption').Text := FCaption;
  Node.AddChild('Category').Text := FCategory;
  Node.AddChild('ShortCut').Text := FShortCut;
  Node.AddChild('SecondaryShortCut').Text := FSecondaryShortCut;
  Node.AddChild('Hint').Text := FHint;
  FActionComponentInfo.SaveToXML(Node.AddChild('ActionComponentInfo'));
end;

{ TComponentPathInfo }

type
  TControlHelper = class helper for TControl
    function GetControlText: string;
  end;

procedure TComponentPathInfo.FillInfo(AComponent: TComponent);
begin
  FComponentName := AComponent.Name;
  FComponentClassName := AComponent.ClassName;
  if AComponent is TControl then
    FComponentCaption := TControl(AComponent).GetControlText;
end;

procedure TComponentPathInfo.SaveToXML(Node: IXMLNode);
begin
  inherited;
  Node.AddChild('ComponentName').Text := FComponentName;
  Node.AddChild('ComponentClassName').Text := FComponentClassName;
  Node.AddChild('ComponentCaption').Text := FComponentCaption;
end;

function TComponentPathInfo.ToString: string;
begin
  Result := Format('%s (%s) [%s]', [FComponentName, FComponentClassName, FComponentCaption]);
end;

{ TControlHelper }

function TControlHelper.GetControlText: string;
begin
  Result := Caption;
end;

{ TComponentPathInfoList }

procedure TComponentPathInfoList.FillComponentInfo(AComponent: TComponent);
var
  Item: TComponentPathInfo;
begin
  Clear;
  while Assigned(AComponent) do
  begin
    Item := TComponentPathInfo.Create;
    try
      Item.FillInfo(AComponent);
      Add(Item);
      Item := nil;
    finally
      Item.Free;
    end;

    AComponent := AComponent.Owner;
  end;
end;

procedure TComponentPathInfoList.FillControlInfo(AComponent: TComponent);
var
  AControl: TControl;
  Item: TComponentPathInfo;
begin
  Clear;
  if not(AComponent is TControl) then
    exit;

  AControl := TControl(AComponent);
  while Assigned(AControl) do
  begin
    Item := TComponentPathInfo.Create;
    try
      Item.FillInfo(AControl);
      Add(Item);
      Item := nil;
    finally
      Item.Free;
    end;

    AControl := AControl.Parent;
  end;
end;

function TComponentPathInfoList.ToString: string;
var
  i: integer;
begin
  Result := '';
  for i := Count - 1 downto 0 do
    Result := Result + ' \ ' + Items[i].ToString;
end;

end.
