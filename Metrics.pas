unit Metrics;

interface

uses
  System.Classes,
  Vcl.ActnList;

type
  TActionList = class(Vcl.ActnList.TActionList)
  strict private
    FAdditionalInfo: TStrings;
  public
    class procedure DirectAddMetrics(AAction: TBasicAction; AAdditionalInfo: TStrings); overload;
    class procedure DirectAddMetrics(AAction: TBasicAction; const AAdditionalInfo: string); overload;
    class procedure DirectAddMetrics(AAction: TBasicAction; const AAdditionalInfo: array of string); overload;

    class procedure DirectAddMetricsByComponent(AComponent: TComponent; AAdditionalInfo: TStrings); overload;
    class procedure DirectAddMetricsByComponent(AComponent: TComponent; const AAdditionalInfo: string); overload;
    class procedure DirectAddMetricsByComponent(AComponent: TComponent;
      const AAdditionalInfo: array of string); overload;

    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    function ExecuteAction(Action: TBasicAction): Boolean; override;

    property AdditionalInfo: TStrings read FAdditionalInfo;
  end;

implementation

uses
  Metrics.Classes;

{ TActionList }

constructor TActionList.Create(AOwner: TComponent);
begin
  inherited;
  FAdditionalInfo := TStringList.Create;
end;

destructor TActionList.Destroy;
begin
  FAdditionalInfo.Free;
  inherited;
end;

class procedure TActionList.DirectAddMetrics(AAction: TBasicAction; AAdditionalInfo: TStrings);
begin
  TMetricsThread.DirectAddMetrics(AAction, AAdditionalInfo);
end;

class procedure TActionList.DirectAddMetrics(AAction: TBasicAction; const AAdditionalInfo: string);
begin
  DirectAddMetrics(AAction, [AAdditionalInfo]);
end;

class procedure TActionList.DirectAddMetrics(AAction: TBasicAction; const AAdditionalInfo: array of string);
var
  SL: TStringList;
  i: Integer;
begin
  SL := TStringList.Create;
  try
    for i := low(AAdditionalInfo) to high(AAdditionalInfo) do
      SL.Add(AAdditionalInfo[i]);
    DirectAddMetrics(AAction, SL);
  finally
    SL.Free;
  end;
end;

class procedure TActionList.DirectAddMetricsByComponent(AComponent: TComponent; const AAdditionalInfo: string);
begin
  DirectAddMetricsByComponent(AComponent, [AAdditionalInfo]);
end;

class procedure TActionList.DirectAddMetricsByComponent(AComponent: TComponent; const AAdditionalInfo: array of string);
var
  SL: TStringList;
  i: Integer;
begin
  SL := TStringList.Create;
  try
    for i := low(AAdditionalInfo) to high(AAdditionalInfo) do
      SL.Add(AAdditionalInfo[i]);
    DirectAddMetricsByComponent(AComponent, SL);
  finally
    SL.Free;
  end;
end;

class procedure TActionList.DirectAddMetricsByComponent(AComponent: TComponent; AAdditionalInfo: TStrings);
begin
  TMetricsThread.DirectAddMetricsByComponent(AComponent, AAdditionalInfo);
end;

function TActionList.ExecuteAction(Action: TBasicAction): Boolean;
begin
  AdditionalInfo.Clear;
  Result := inherited;
  DirectAddMetrics(Action, FAdditionalInfo);
end;

end.
