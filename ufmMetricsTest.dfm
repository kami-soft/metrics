object Form17: TForm17
  Left = 0
  Top = 0
  Caption = 'Form17'
  ClientHeight = 506
  ClientWidth = 771
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object mmo1: TMemo
    Left = 0
    Top = 0
    Width = 684
    Height = 506
    Margins.Right = 100
    Align = alClient
    Lines.Strings = (
      'mmo1')
    TabOrder = 0
  end
  object pnl1: TPanel
    Left = 684
    Top = 0
    Width = 87
    Height = 506
    Align = alRight
    Caption = 'pnl1'
    TabOrder = 1
    DesignSize = (
      87
      506)
    object btn1: TButton
      Left = 4
      Top = 402
      Width = 75
      Height = 25
      Action = act1
      Anchors = [akRight, akBottom]
      TabOrder = 0
    end
    object btn2: TButton
      Left = 4
      Top = 433
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = 'btn2'
      TabOrder = 1
      OnClick = btn2Click
    end
  end
  object actlst1: TActionList
    OnExecute = actlst1Execute
    Left = 456
    Top = 56
    object act1: TAction
      Caption = 'Action1'
      ShortCut = 16450
      OnExecute = act1Execute
    end
  end
end
